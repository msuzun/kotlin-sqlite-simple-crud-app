package com.example.sqlitesimplecrud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast


import com.example.sqlitesimplecrud.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val context = this
        var db = DataBaseHelper(context)
        binding.buttonKaydet.setOnClickListener {
            var editadsoyad = binding.editTextName.text.toString()
            var edityas = binding.editTextYas.text.toString()
            if (editadsoyad.isNotEmpty() && edityas.isNotEmpty()){
                var kullanici = Kullanici(editadsoyad,edityas.toInt())
                db.insertData(kullanici)
            }
            else{
                Toast.makeText(applicationContext,"Lütfen boş alanları doldurunuz",Toast.LENGTH_SHORT).show()
            }
        }
        // Verileri okumak için
        binding.buttonOkuma.setOnClickListener {
            var data = db.readData()
            binding.textViewSonuc.text = ""
            for (i in 0 until data.size){
                binding.textViewSonuc.append(data.get(i).id.toString()+" " + data.get(i).adsoyad + " "+ data.get(i).yasi + "\n")
            }
        }
        // Verileri güncellemek için
        binding.buttonGuncelle.setOnClickListener {
            db.updateData()
            binding.buttonOkuma.performClick()
        }
        // Verileri silmek için
        binding.buttonSil.setOnClickListener {
            db.deleteData()
            binding.buttonOkuma.performClick()
        }
    }
}